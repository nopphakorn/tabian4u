<?php include "../connectdb.php";
// include "logindb.php";?>
<!DOCTYPE html>
<html lang="th">
  <head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ปิดบัญชี</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
	  <style>
  .modal-header, h4, .close {
      background-color: #5cb85c;
      color:white !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-footer {
      background-color: #f9f9f9;
  }

  .date {
    padding-right: 0px;
    /*padding-left: 0px;*/
	padding-top: 6px;
	
}
  </style>

  </head>
  

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><i class="fa fa-desktop"></i> <span>ยินดีต้อนรับ</span></a>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
			<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
				<div class="menu_section">
					<?php include "menu.php"; ?>
				</div>
			</div>
            <!-- /sidebar menu -->

           
          </div>
        </div>

        
         <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <?php include "nav.php"; ?>
          </div>
        </div>
        <!-- /top navigation -->
		

   

     

        <!-- page content -->
	
        <div class="right_col" role="main">
          <div class="">
			<div class="page-title">
				<div class="col-md-12 col-sm-12 col-xs-12">
			  
					<center>
							<!-- <a class="btn icon-btn btn-primary" href="form.php?">
							<span class="glyphicon glyphicon-plus"> เพิ่มข้อมูลเครื่องคอมพิวเตอร์</span>
							</a> -->
					
							<!-- <a class="btn icon-btn btn-info" href="print_qr.php?">
							<span class="glyphicon glyphicon-qrcode"> พิมพ์คิวอาร์โค้ดทั้งหมด &nbsp;</span>
							</a> -->
					</center>


					<div class="row">
				  <br><br>
				
					<div class="">
					<form  method="get" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">
					<div class="col-md-2"></div>
					<div class="col-md-1 col-xs-3"><label class="control-label "> ค้นหาโดย </label></div>
					<div class="col-md-2 col-xs-6">
                          <select class="form-control" name="select">
						  <option    value="1" <?php  if($_GET['select'] =="1"){echo "selected";}else{echo "";}?>>เลขที่สัญญา</option>
                          <option    value="2" <?php if($_GET['select'] =="2"){echo "selected";}else{echo "";}?>>ทะเบียนรถ</option>
                          </select>
					</div>
					<div class="col-md-3 "><input type="text" name="search" class="form-control" value="<?php echo $_GET["search"];?>" placeholder="กรุณาใส่ข้อมูลการค้นหา ..."></div>
					<div class="col-md-1"><button class="btn btn-default" type="submit">ค้นหา</button></div>
					<div class="col-md-2"></div>
					</form>
					</div><!-- /input-group -->
				 
				</div>

	<?php
$search = $_GET["search"];
if($_GET["select"]=='1'){
							$sql = "SELECT DISTINCT TOP 1
										dbo.HPREG.HP_ID as HP_ID ,
										dbo.HPREG.HP_CTM_NAME as HP_CTM_NAME ,
										dbo.HPREG.HP_BRAN_ID as HP_BRAN_ID,
										dbo.HPREGASS.ASS_NO as ASS_NO

										FROM
										dbo.HPREG
										LEFT JOIN dbo.BRANMAS ON dbo.BRANMAS.BRAN_ID = dbo.HPREG.HP_BRAN_ID
										LEFT JOIN dbo.HPREGASS ON dbo.HPREGASS.HP_ID = dbo.HPREG.HP_ID
										WHERE
										dbo.HPREG.HP_ID like  '%".@$search."%' ";
	$stmt = sqlsrv_query( $conn, $sql );
	 }
						
						if($_GET["select"]=='2'){
							$sql = "SELECT DISTINCT TOP 1
										dbo.HPREG.HP_ID as HP_ID ,
										dbo.HPREG.HP_CTM_NAME as HP_CTM_NAME ,
										dbo.HPREG.HP_BRAN_ID as HP_BRAN_ID,
										dbo.HPREGASS.ASS_NO as ASS_NO

										FROM
										dbo.HPREG
										LEFT JOIN dbo.BRANMAS ON dbo.BRANMAS.BRAN_ID = dbo.HPREG.HP_BRAN_ID
										LEFT JOIN dbo.HPREGASS ON dbo.HPREGASS.HP_ID = dbo.HPREG.HP_ID
										WHERE
										dbo.HPREGASS.ASS_NO like  '%".@$search."%' ";
										$stmt = sqlsrv_query( $conn, $sql );
						
						} 

						
	?>
                <div class="x_panel">
                  <div class="x_title">  
				  พิมพ์ชุดปิดบัญชี
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-responsive1" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
						  
                          <th width ='25%' >ชือผู้เช่าซื้อ</th>
                          <th width ='25%' >สัญญา</th>
						  <th width ='25%' >ทะเบียนรถ</th>
                          <th width ='5%' >พิมพ์</th>
                        </tr>
                      </thead>
                     
											<?php
											if( $stmt === false) {
													die( print_r( sqlsrv_errors(), true) );
												}
											$i  = 1;
												while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
												
											?>
                        <tr>
                         
                          <td><?php echo $row["HP_CTM_NAME"];?></td>
                          <td><?php echo $row["HP_ID"];?></td>
                          <td><?php echo $row["ASS_NO"];?></td>
						  
						  
				<td>		
					<center><a  id="myBtn" class="btn icon-btn btn-default" >
					<span class="glyphicon glyphicon-print img-circle text-info  "></span>
					</a></center>
				</td>
				
				</tr>


					
						  <div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog">
							
							  <!-- Modal content-->
							  <div class="modal-content" style="width: 702px;">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4></span> <?php echo $row["HP_ID"];?></h4>
								  
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_close_acc.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-2 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day" placeholder="วันที่">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
										
											</div>
											
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year" placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
											
										</div>
									</div>
									<hr>
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-2 date"></span>ผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div>
											
											
											
										</div>
									</div>

									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-2 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att'></textarea>
											</div>
										</div>
									</div>
								
								
									
									
									 
								 
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-print img-circle text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								//var A ='<?php  echo "#myBtn".$Z?>'; 
								//var B = '<?php  echo "#myModal".$i ?>' ;  
								$(document).ready(function(){
									$('#myBtn').click(function(){
										$('#myModal').modal();
									});
								});
								 //alert (A); // แบบนี้ก็ใช้เช่นกันครับ 
								
								</script>
                        	
                                        
											<?php
								
											}	
											
											sqlsrv_free_stmt( $stmt);
											?>
                      
                    </table>

                  </div>
                </div>
              </div>
            </div>



      
    </div>
  </div> 
</div>
 







          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->





  </body>
</html>