<?php 
include "../logindb.php";
// include "logindb.php";?>
<!DOCTYPE html>
<html lang="th">
  <head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ชุดปิดจัดใหม่</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
	  <link href="../vendors/bootstrap/dist/css/color.css" rel="stylesheet">
  </head>
  

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
               <?php include 'show_session.php';?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
			<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
				<div class="menu_section">
					<?php include "menu.php"; ?>
				</div>
			</div>
            <!-- /sidebar menu -->

           
          </div>
        </div>

        
         <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <?php include "nav.php"; ?>
          </div>
        </div>
        <!-- /top navigation -->
		

   

     

        <!-- page content -->
	
        <div class="right_col" role="main">
          <div class="">
			<div class="page-title">
				<div class="col-md-12 col-sm-12 col-xs-12">
			  
					<center>
							<!-- <a class="btn icon-btn btn-primary" href="form.php?">
							<span class="glyphicon glyphicon-plus"> เพิ่มข้อมูลเครื่องคอมพิวเตอร์</span>
							</a> -->
					
							<!-- <a class="btn icon-btn btn-info" href="print_qr.php?">
							<span class="glyphicon glyphicon-qrcode"> พิมพ์คิวอาร์โค้ดทั้งหมด &nbsp;</span>
							</a> -->
					</center>


					<div class="row">
				  <br><br>
				
				<?php include "search.php";?>
				 
				</div>

	<?php
$search = $_GET["search"];
if($_GET["select"]=='1'){
							$sql = "SELECT DISTINCT TOP 1
										dbo.HPREG.HP_ID as HP_ID ,
										dbo.HPREG.HP_CTM_NAME as HP_CTM_NAME ,
										dbo.HPREG.HP_BRAN_ID as HP_BRAN_ID,
										dbo.HPREGASS.ASS_NO as ASS_NO

										FROM
										dbo.HPREG
										LEFT JOIN dbo.BRANMAS ON dbo.BRANMAS.BRAN_ID = dbo.HPREG.HP_BRAN_ID
										LEFT JOIN dbo.HPREGASS ON dbo.HPREGASS.HP_ID = dbo.HPREG.HP_ID
										WHERE
										dbo.HPREG.HP_ID like  '%".@$search."%' ";
	$stmt = sqlsrv_query( $conn, $sql );
	 }
						
						if($_GET["select"]=='2'){
							$sql = "SELECT DISTINCT TOP 1
										dbo.HPREG.HP_ID as HP_ID ,
										dbo.HPREG.HP_CTM_NAME as HP_CTM_NAME ,
										dbo.HPREG.HP_BRAN_ID as HP_BRAN_ID,
										dbo.HPREGASS.ASS_NO as ASS_NO

										FROM
										dbo.HPREG
										LEFT JOIN dbo.BRANMAS ON dbo.BRANMAS.BRAN_ID = dbo.HPREG.HP_BRAN_ID
										LEFT JOIN dbo.HPREGASS ON dbo.HPREGASS.HP_ID = dbo.HPREG.HP_ID
										WHERE
										dbo.HPREGASS.ASS_NO like  '%".@$search."%' ";
										$stmt = sqlsrv_query( $conn, $sql );
						
						} 

						
	?>
                <div class="x_panel">
                  <div class="x_title">  
				  พิมพ์ชุดปิดจัดใหม่
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable-responsive1" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
						  
                          <th width ='25%' >ชือผู้เช่าซื้อ</th>
                          <th width ='25%' >สัญญา</th>
						  <th width ='25%' >ทะเบียนรถ</th>
                          <th width ='5%' >พิมพ์</th>
                        </tr>
                      </thead>
                     
											<?php
											if( $stmt === false) {
													die( print_r( sqlsrv_errors(), true) );
												}
											$i  = 1;
												while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
												
											?>
                        <tr>
                         
                          <td><?php echo $row["HP_CTM_NAME"];?></td>
                          <td><?php echo $row["HP_ID"];?></td>
                          <td><?php echo $row["ASS_NO"];?></td>
						  
						  
				<td>		
					<div class='btn-group-vertical'>
							
									<button  id="myBtn" class="btn icon-btn btn-default"  data-placement="top" data-toggle="tooltip" title="เอกสารเรียงตั้งแต่แผ่นที่ 1 - 7 ">
										<span class="glyphicon glyphicon-print img-circle text-info left  "> พิมพ์เอกสารทั้งหมด</span>
									</button> 
							
									<button  id="myBtn1" class="btn icon-btn btn-default" >
										<span class="glyphicon  img-circle text-info left  "> 1 แบบคำขอโอนรับโอน (บริษัทเป็นผู้โอน)</span>
									</button>
									<button  id="myBtn2" class="btn icon-btn btn-default" >
										<span class="glyphicon  img-circle text-info left  "> 2 แบบคำขอโอนรับโอน (ลูกค้าเป็นผู้รับโอน)</span>
									</button>
							
									<button  id="myBtn3" class="btn icon-btn btn-default" >
										<span class="glyphicon  img-circle text-info  left"> 3 หนังสือรับรองการชำระเงิน</span>
									</button>
										<button  id="myBtn4" class="btn icon-btn btn-default" >
										<span class="glyphicon  img-circle text-info  left"> 4 หนังสือรับรองการเช่าซื้อ</span>
									</button>
									<button  id="myBtn5" class="btn icon-btn btn-default" >
										<span class="glyphicon  img-circle text-info left "> 5 หนังสือรับรองการปิดบัญชีและชำระภาษีฯ</span>
									</button>
									<button  id="myBtn6" class="btn icon-btn btn-default" >
										<span class="glyphicon  img-circle text-info left "> 6 หนังสือมอบอำนาจ (ลูกค้า)</span>
									</button>
							
									<button  id="myBtn7" class="btn icon-btn btn-default" >
										<span class="glyphicon  img-circle text-info left "> 7 หนังสือมอบอำนาจ (บริษัท)</span>
									</button>
							</div>
				</td>
				
				</tr>


					
						  <div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								    <h4><?php echo $row["HP_ID"];?></h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day" min='1' max='31'placeholder="วันที่"required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year" min='1'  placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div>
									<?php } ?>
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" min='1' max='31' placeholder="วันที่" required="required">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" min='1'  value='2561'>
											</div>
										</div>
									</div>
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" min='1'  placeholder="อายุ">
											</div>
										</div>
									</div>

									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  ></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn').click(function(){
										$('#myModal').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>


						 <div class="modal fade" id="myModal1" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4>แบบคำขอโอนรับโอน (บริษัท)</h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew1.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day" min='1' max='31' placeholder="วันที่" required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year"min='1'  placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div>
									<?php }?>
									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" placeholder="วันที่">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div> -->
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<!-- <div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div> -->
										</div>
									</div>

									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  required="required"></textarea>
											</div>
										</div>
									</div> -->
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn1').click(function(){
										$('#myModal1').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>
                        	

							
						  <div class="modal fade" id="myModal2" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4>แบบคำขอโอนรับโอน (ลูกค้า)</h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew2.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day" min='1' max='31' placeholder="วันที่" required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year" placeholder="ปี (พ.ศ.)" min='1'value='2561'>
											</div>
										</div>
									</div>
									<?php }?>
									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" placeholder="วันที่">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div> -->
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<!-- <div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div> -->
										</div>
									</div>

									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  required="required"></textarea>
											</div>
										</div>
									</div> -->
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn2').click(function(){
										$('#myModal2').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>



								
						  <div class="modal fade" id="myModal3" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4>หนังสือรับรองการชำระเงิน</h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew3.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day"min='1' max='31' placeholder="วันที่" required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year" min='1' placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div>
									<?php }?>
									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" placeholder="วันที่">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div>
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div>
										</div>
									</div>

									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  required="required"></textarea>
											</div>
										</div>
									</div> -->
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn3').click(function(){
										$('#myModal3').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>

								
						  <div class="modal fade" id="myModal4" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4>หนังสือรับรองการเช่าซื้อ</h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew4.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day"min='1' max='31' placeholder="วันที่" required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year" placeholder="ปี (พ.ศ.)" min='1' value='2561'>
											</div>
										</div>
									</div>
									<?php }?>
									<!--<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" min='1' max='31' placeholder="วันที่" required="required">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" min='1'  value='2560'>
											</div>
										</div>
									</div -->
									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div>
										</div>
									</div>

									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  required="required"></textarea>
											</div>
										</div>
									</div> -->
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn4').click(function(){
										$('#myModal4').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>


								
						  <div class="modal fade" id="myModal5" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4>หนังสือรับรองการปิดบัญชีและชำระภาษีฯ</h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew5.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day" min='1' max='31' placeholder="วันที่" required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year" placeholder="ปี (พ.ศ.)"min='1' value='2561'>
											</div>
										</div>
									</div>
									<?php }?>
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" min='1' max='31' placeholder="วันที่" required="required">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" min='1' value='2560'>
											</div>
										</div>
									</div>
									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div>
										</div>
									</div>

									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  required="required"></textarea>
											</div>
										</div>
									</div> -->
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn5').click(function(){
										$('#myModal5').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>
                                        


										
						  <div class="modal fade" id="myModal6" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4>หนังสือมอบอำนาจ (ลูกค้า)</h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew6.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day"  min='1' max='31' placeholder="วันที่" required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year" placeholder="ปี (พ.ศ.)" min='1'  value='2561'>
											</div>
										</div>
									</div>
									<?php }?>
									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" placeholder="วันที่">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div> -->
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div>
										</div>
									</div>

									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  ></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn6').click(function(){
										$('#myModal6').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>


								
						  <div class="modal fade" id="myModal7" role="dialog">
							<div class="modal-dialog">
							  <!-- Modal content-->
							  <div class="modal-content bg-modal">
								<div class="modal-header" style="padding:10px 50px;">
								  <button type="button" class="close" data-dismiss="modal">&times;</button>
								  <h4>หนังสือมอบอำนาจ (บริษัท)</h4>
								</div>
								<div class="modal-body" style="padding:40px 50px;">
								  <form  action='print_renew7.php?id_hpreg=<?php echo $row["HP_ID"];?>' method="post">
								  <input type="hidden"  value='<?php echo $row["HP_ID"];?>'  name="HP_ID">
								  
								  <?php  if(		$_SESSION["USER_CODE"] =='0126'  
												or		$_SESSION["USER_CODE"] =='0128'  
												or		$_SESSION["USER_CODE"] =='0102'  
												or		$_SESSION["USER_CODE"] =='0098' 
												){
									}else{
									?>

									
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่โอน</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day" min='1' max='31' placeholder="วันที่" required="required">
											</div>
											<div class='col-md-3'>
											<select class="form-control" name="month">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year"min='1'  placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div>
									<?php }?>
									<!-- <div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> วันที่ของสัญญาเก่า</label>
											<div class='col-md-3'>
											<input type="number" class="form-control  " name="day_before" placeholder="วันที่">
											</div>
										
											<div class='col-md-3'>
											<select class="form-control" name="month_before">
												  <option    value="มกราคม" >มกราคม</option>
												  <option    value="กุมภาพันธ์" >กุมภาพันธ์</option>
												  <option    value="มีนาคม" >มีนาคม</option>
												  <option    value="เมษายน" >เมษายน</option>
												  <option    value="พฤษภาคม" >พฤษภาคม </option>
												  <option    value="มิถุนายน" >มิถุนายน</option>
												  <option    value="กรกฎาคม" >กรกฎาคม</option>
												  <option    value="สิงหาคม" >สิงหาคม</option>
												  <option    value="กันยายน" >กันยายน</option>
												  <option    value="ตุลาคม" >ตุลาคม</option>
												  <option    value="พฤศจิกายน" >พฤศจิกายน</option>
												  <option    value="ธันวาคม" >ธันวาคม</option>
											</select>
											</div>
											<div class='col-md-3'>
											<input type="number" class="form-control col-md-1" name="year_before" placeholder="ปี (พ.ศ.)" value='2561'>
											</div>
										</div>
									</div> -->
									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span> ชื่อผู้ทำเรื่องโอน</label>
											<div class='col-md-4'>
											<input type="text" class="form-control  " name="name_transfer" placeholder="ชื่อ นามสกุล">
											</div>
										
											<div class='col-md-2'>
											<input type="number" class="form-control " name="old_att" placeholder="อายุ">
											</div>
										</div>
									</div>

									<div class="form-group row ">
										<div class='col-md-12 '>
											<label for="day" class="col-md-3 date"></span>บ้านเลขที่</label>
											<div class='col-md-6'>
											<textarea rows="2" cols="60" class="form-control"  name='address_att' data-toggle="tooltip" data-placement="right" title='ตัวอย่าง เช่น
											" 459/1-4 ม.7 ถ.มิตรภาพ  ต.สมอแข อ.เมือง จ.พิษณุโลก " '  ></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-block  btn-round "><span class="glyphicon glyphicon-print img-circle white text-info  "></span> พิมพ์</button>
								</div>
								 </form>
								<script>
								$(document).ready(function(){
									$('#myBtn7').click(function(){
										$('#myModal7').modal();
									});
								});
								</script>
										</div>
									</div>
								</div>
											<?php
								
											}	
											
											sqlsrv_free_stmt( $stmt);
											?>
                      
                    </table>

                  </div>
                </div>
              </div>
            </div>



      
    </div>
  </div> 
</div>
 







          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->





  </body>
</html>